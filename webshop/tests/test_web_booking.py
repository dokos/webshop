from frappe.tests import IntegrationTestCase
from frappe.utils import getdate as D

from webshop.webshop.shopping_cart.booking import get_period_for_date as G


class TestGetPeriodForDate(IntegrationTestCase):
	def test_period_month(self):
		self.assertEqual(G(D("2021-01-01"), "Month", 1), (D("2021-01-01"), D("2021-01-31")))
		self.assertEqual(G(D("2021-01-06"), "Month", 2), (D("2021-01-01"), D("2021-02-28")))
		self.assertEqual(G(D("2021-01-11"), "Month", 3), (D("2021-01-01"), D("2021-03-31")))
		self.assertEqual(G(D("2021-01-16"), "Month", 4), (D("2021-01-01"), D("2021-04-30")))
		self.assertEqual(G(D("2021-01-21"), "Month", 5), (D("2021-01-01"), D("2021-05-31")))
		self.assertEqual(G(D("2021-01-28"), "Month", 6), (D("2021-01-01"), D("2021-06-30")))
		self.assertEqual(G(D("2021-01-29"), "Month", 7), (D("2021-01-01"), D("2021-07-31")))
		self.assertEqual(G(D("2021-01-30"), "Month", 8), (D("2021-01-01"), D("2021-08-31")))
		self.assertEqual(G(D("2021-01-31"), "Month", 9), (D("2021-01-01"), D("2021-09-30")))
		self.assertEqual(G(D("2021-01-31"), "Month", 10), (D("2021-01-01"), D("2021-10-31")))
		self.assertEqual(G(D("2021-01-31"), "Month", 11), (D("2021-01-01"), D("2021-11-30")))
		self.assertEqual(G(D("2021-01-31"), "Month", 12), (D("2021-01-01"), D("2021-12-31")))
		self.assertEqual(G(D("2021-01-31"), "Month", 13), (D("2021-01-01"), D("2022-01-31")))

	def test_period_week(self):
		self.assertEqual(G(D("2018-01-01"), "Week", 1), (D("2018-01-01"), D("2018-01-07")))
		self.assertEqual(G(D("2019-01-01"), "Week", 1), (D("2018-12-31"), D("2019-01-06")))
		self.assertEqual(G(D("2020-01-01"), "Week", 1), (D("2019-12-30"), D("2020-01-05")))
		self.assertEqual(G(D("2021-01-01"), "Week", 1), (D("2020-12-28"), D("2021-01-03")))
		self.assertEqual(G(D("2022-01-01"), "Week", 1), (D("2021-12-27"), D("2022-01-02")))
		self.assertEqual(G(D("2023-01-01"), "Week", 1), (D("2022-12-26"), D("2023-01-01")))
