import frappe

from frappe.utils import flt

from erpnext.selling.doctype.sales_order.sales_order import SalesOrder

class WebshopSalesOrder(SalesOrder):
	def get_redirect_url(self, shopping_cart_settings):
		if (
			shopping_cart_settings.authorized_modes_of_payment
			and not shopping_cart_settings.no_payment_gateway
			and (flt(self.rounded_total or self.grand_total) - flt(self.advance_paid))
			> 0.0
		):
			from erpnext.accounts.doctype.payment_request.payment_request import make_payment_request
			if existing_pr := frappe.db.exists("Payment Request", dict(reference_doctype=self.doctype, reference_name=self.name, docstatus=1)):
				return frappe.db.get_value("Payment Request", existing_pr, "payment_url")
			else:
				pr = make_payment_request(
					dn=self.name,
					dt=self.doctype,
					submit_doc=1,
					order_type="Shopping Cart",
					return_doc=1,
				)

				return pr.payment_url
