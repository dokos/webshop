from datetime import timedelta
from typing import TYPE_CHECKING, Literal

import frappe
from frappe import _
from frappe.utils import add_to_date, cint, get_start_of_week_index, getdate, sbool
from frappe.utils.data import convert_utc_to_system_timezone, get_datetime

from webshop.webshop.shopping_cart.cart import get_party, update_cart

if TYPE_CHECKING:
	from bookings.bookings.doctype.venue_settings.venue_settings import VenueSettings

	from webshop.webshop.doctype.website_item.website_item import WebsiteItem


def normalize_utc_to_system(s: str) -> str:
	if s.endswith("Z"):
		d = get_datetime(s.rstrip("Z"))
		assert d
		d = convert_utc_to_system_timezone(d)
		d = d.replace(tzinfo=None)
		return d.isoformat()
	return s


@frappe.whitelist()
def book_new_slot(
	web_item: str,
	start: str,
	end: str,
	uom: str,
	with_credits=False,
	skip_cart=0,
	all_day=0,
	quantity=1,
	**kwargs,
):
	start = normalize_utc_to_system(start)
	end = normalize_utc_to_system(end)

	quantity = cint(quantity) or 1
	add_to_cart = True
	skip_cart = sbool(skip_cart)
	with_credits = sbool(with_credits)
	all_day = sbool(all_day)

	try:
		if frappe.db.exists("Website Item", web_item):
			website_item: "WebsiteItem" = frappe.get_doc("Website Item", web_item)  # type: ignore
		else:
			website_item: "WebsiteItem" = frappe.get_doc("Website Item", {"item_code": web_item})  # type: ignore

		if not website_item.published:
			frappe.throw(frappe._("Not Published"))

		check_badges(website_item.name)

		status = "In cart"
		if skip_cart:
			# Frontend wants to skip cart
			if website_item.can_skip_cart(uom=uom):
				# ...and is allowed to.
				status = "Confirmed"
				add_to_cart = False

		party = get_party()

		params = {
			"doctype": "Item Booking",
			"item": website_item.item_code,
			"starts_on": start,
			"ends_on": end,
			"user": frappe.session.user,
			"party_type": party.doctype if party else None,
			"party_name": party.name if party else None,
			"status": status,
			"all_day": all_day,
			"uom": uom,
			"sync_with_google_calendar": frappe.db.get_single_value(
				"Venue Settings", "sync_with_google_calendar"
			),
			"deduct_booking_credits": with_credits,
		}

		doc = frappe.get_doc(params)
		doc.insert(ignore_permissions=True)

		if add_to_cart:
			update_cart(website_item.item_code, quantity or 1, uom=uom, booking=doc.name)
		return doc.name
	except Exception:
		if frappe.conf.developer_mode:
			raise

		frappe.log_error()
		frappe.throw(frappe._("New item booking error"))


# @frappe.whitelist()
# def is_slot_available(web_item: str, start: str, end: str, all_day=0, **kwargs):
# 	website_item: "WebsiteItem" = frappe.get_doc("Website Item", web_item)  # type: ignore

# 	if not website_item.published:
# 		frappe.throw(frappe._("Not Published"))

# 	ib: "ItemBooking" = frappe.new_doc("Item Booking")  # type: ignore
# 	ib.item = website_item.item_code
# 	ib.starts_on = start
# 	ib.ends_on = end
# 	ib.all_day = all_day

# 	try:
# 		ib.check_overlaps()
# 	except frappe.ValidationError:
# 		return False
# 	return True


@frappe.whitelist()
def book_long_slot(
	web_item: str,
	start_date: str,
	uom: str,
	count: int = 1,
	quantity: int = 1,
	skip_cart: bool = False,
):
	# Clean-up parameters
	count = cint(count) or 1
	quantity = cint(quantity) or 1
	skip_cart = bool(cint(skip_cart))

	start = getdate(start_date)
	if not start:
		return frappe.throw(frappe._("Invalid Start Date"))

	# Validate chosen UOM
	venue_settings: "VenueSettings" = frappe.get_cached_doc("Venue Settings")  # type: ignore
	uom_infos = venue_settings.get_uom_infos()
	conversion = None
	for info in uom_infos.values():
		if info.from_uom == uom:
			conversion = info
			break

	if not conversion:
		# The UOM was not found
		return frappe.throw(f"Invalid UOM: {uom!r}")

	# Ensure count is multiple of conversion factor
	if (count % conversion.value) != 0:
		return frappe.throw(
			f"Invalid Count: got {count!r} expected multiple of {conversion.value!r} [error code: {conversion!r}]"
		)
	final_quantity = quantity * (count // conversion.value)

	if conversion.target_type == "Minute":
		return frappe.throw(f"Invalid UOM: {uom!r}, should be Day, Week, Month or Year")

	start, end = get_period_for_date(start, conversion.target_type, count)

	return book_new_slot(
		web_item=web_item,
		start=start.isoformat(),
		end=end.isoformat(),
		uom=uom,
		count=count,
		quantity=final_quantity,
		all_day=1,
		skip_cart=skip_cart,
	)


def get_period_for_date(date, period: Literal["Day", "Week", "Month", "Year"], count: int):
	"""
	Returns a period start and end date for a given date.
	The given date is included in the period.
	"""
	period_start = getdate(date)

	if not period_start:
		raise ValueError("Invalid date")

	if count < 1:
		raise ValueError("Invalid count")

	period_end = period_start

	# Ensure start of period is correct
	if period == "Day":
		# do nothing to start date
		period_end = add_to_date(period_start, days=count)
	elif period == "Week":
		# monday - monday
		sow = get_start_of_week_index() - 1
		days_after_monday = period_start.weekday()
		days_till_start = (14 + days_after_monday - sow) % 7
		period_start = add_to_date(period_start, days=-days_till_start)  # Ensure day is start of week
		period_end = add_to_date(period_start, days=7 * count) + timedelta(days=-1)
	elif period == "Month":
		period_start = period_start.replace(day=1)
		period_end = add_to_date(period_end, months=count).replace(day=1) + timedelta(days=-1)
	elif period == "Year":
		period_start = period_start.replace(month=1, day=1)
		period_end = add_to_date(period_end, years=count).replace(month=1, day=1) + timedelta(days=-1)
	else:
		raise ValueError("Invalid period")

	return period_start, period_end


def check_badges(website_item):
	if not frappe.db.exists("DocType", "Booking Resource"):
		return

	resource = frappe.get_cached_doc("Booking Resource", dict(website_item=website_item))
	if resource_badges := [b.badge_type for b in resource.badges]:
		user_badges = frappe.get_all(
			"Booking Badge",
			filters={"user": frappe.session.user, "status": "Active", "docstatus": 1},
			pluck="badge_type",
			distinct=True,
		)

		if diff := list(set(resource_badges) - set(user_badges)):
			frappe.throw(
				_("In order to book this resource you need the following badges: {0}").format(" ,".join(diff))
			)
