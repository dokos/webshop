import frappe

def get_authorized_modes_of_payment(company=None):
	webshop_settings = frappe.get_cached_doc("Webshop Settings")
	enabled_modes_of_payment = [
		mop.mode_of_payment for mop in webshop_settings.authorized_modes_of_payment
	]

	mode_of_payment = frappe.qb.DocType("Mode of Payment")
	mode_of_payment_account = frappe.qb.DocType("Mode of Payment Account")
	payment_gateway = frappe.qb.DocType("Payment Gateway")
	query = (
		frappe.qb.from_(mode_of_payment_account)
		.left_join(mode_of_payment)
		.on(mode_of_payment_account.parent == mode_of_payment.name)
		.left_join(payment_gateway)
		.on(mode_of_payment_account.payment_gateway == payment_gateway.name)
		.select(mode_of_payment.icon, mode_of_payment_account.payment_gateway, mode_of_payment.portal_title.as_("title"), mode_of_payment.name, mode_of_payment.portal_description)
		.where(
			mode_of_payment.enabled &
			(mode_of_payment.name.isin(enabled_modes_of_payment)) &
			(mode_of_payment_account.company == company) &
			(payment_gateway.disabled == 0)
		)
	)

	return query.run(as_dict=True)