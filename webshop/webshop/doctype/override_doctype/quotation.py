import frappe
from frappe import _

from erpnext.selling.doctype.quotation.quotation import Quotation

class WebshopQuotation(Quotation):
	def validate(self):
		self.validate_multicompany_items()
		super(WebshopQuotation, self).validate()

	def validate_multicompany_items(self):
		if self.order_type != "Shopping Cart" or not frappe.db.exists("DocType", "Venue Settings"):
			return

		venue_settings = frappe.get_cached_doc("Venue Settings")
		if venue_settings.enable_multi_companies:
			if filter_mc := venue_settings.multicompany_get_item_filter_for_company(self.company):
				kept_items, deleted_items = [], []
				for item in self.items:
					has_compatible_web_item = frappe.get_all(
						"Website Item",
						limit=1,
						filters=[
							["item_code", "=", item.item_code],
							filter_mc,
						],
					)
					if has_compatible_web_item:
						kept_items.append(item)
					else:
						if item.item_booking:
							item.delete(ignore_permissions=True)  # delete the row first to avoid a foreign key error
							frappe.delete_doc("Item Booking", item.item_booking, ignore_permissions=True)
						deleted_items.append(item)

				self.items = kept_items
				if not kept_items:
					frappe.throw(
						_(
							"The shopping cart has been emptied because none of the items are available in the selected company."
						),
						frappe.MandatoryError,
					)
				if deleted_items:
					frappe.msgprint(
						_("This item cannot be added to this shopping cart. Please select another company.")
					)