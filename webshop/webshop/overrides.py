import frappe


def override_webshop_settings(webshop_settings):
	"""
	Hook that returns a new Webshop Settings-like object with overrides for
	the current company (stored in cookies) when the multicompany feature is enabled.
	"""
	return get_shopping_cart_overrides()  # NOTE: might return None, which means no override


def get_shopping_cart_overrides(company=None):
	# Check cache
	if not frappe.db.exists("DocType", "Venue Settings"):
		return

	cached = frappe.cache.get_value(
		"venue_settings_enable_multi_companies",
		generator=lambda: frappe.db.get_single_value("Venue Settings", "enable_multi_companies"),
	)
	if not cached:
		return

	if not hasattr(frappe, "request"):
		return  # not called from a request

	venue_settings = frappe.get_single("Venue Settings")
	if not venue_settings.get("enable_multi_companies"):
		return  # multi-company support is disabled

	company = company or venue_settings.multicompany_get_current_company()
	if not company:
		return  # outside of a valid multi-company context

	# everything is ok, return the overrides (if any, and cached)
	return _get_shopping_cart_overrides_cached(company)


def _get_shopping_cart_overrides_cached(company):
	assert company, "invalid arguments: do not use this function directly"
	venue_settings = frappe.get_cached_doc("Venue Settings")
	cart_settings = frappe.get_cached_doc("Webshop Settings")

	# Find the overrides for the company
	overrides = None
	for o in venue_settings.get("cart_settings_overrides", []):
		if o.get("company") == company:
			overrides = o
			break
	if not overrides:
		return  # note: could've been a for-else, but this is more readable

	# Get and copy the cart_settings to a dict
	cart_settings = cart_settings.as_dict()

	# Fetch the names of the fields to override
	# fields = {"company", "price_list", "default_customer_group", "quotation_series"}
	fields: set[str] = {df.fieldname for df in frappe.get_meta("Venue Cart Settings").fields}
	# NOTE: Could also do an intersection with the fields of the Webshop Settings

	# Remove the _label field as should only be used to display the choice of company
	fields.difference_update({"_label"})

	# Update the base with the overrides
	for fieldname in fields:
		if value := overrides.get(fieldname):
			cart_settings["_" + fieldname + "__original"] = cart_settings[fieldname]
			cart_settings[fieldname] = value

	cart_settings["_was_overridden_by_multicompany_mode"] = True
	cart_settings = frappe._dict(cart_settings)  # convert back to frappe._dict
	return cart_settings
