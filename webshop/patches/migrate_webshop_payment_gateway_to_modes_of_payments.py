import frappe

def execute():
	payment_gateway_account = frappe.db.get_value("Webshop Settings", None, "payment_gateway_account")
	if not payment_gateway_account:
		# Keep for migration from older versions
		payment_gateway_account = frappe.db.get_value("E Commerce Settings", None, "payment_gateway_account")

	if not payment_gateway_account:
		return

	if payment_gateway := frappe.db.get_value("Payment Gateway Account", payment_gateway_account, "payment_gateway"):
		if mop := frappe.db.get_value("Payment Gateway", payment_gateway, "mode_of_payment"):

			mode_of_payment = frappe.get_doc("Mode of Payment", mop)
			if not mode_of_payment.payment_gateway:
				mode_of_payment.payment_gateway = payment_gateway
				if mode_of_payment.accounts:
					row = mode_of_payment.accounts[0]
					row.fee_account = frappe.db.get_value("Payment Gateway", payment_gateway, "fee_account")
					row.tax_account = frappe.db.get_value("Payment Gateway", payment_gateway, "tax_account")
					row.cost_center = frappe.db.get_value("Payment Gateway", payment_gateway, "cost_center")
					mode_of_payment.portal_title = frappe.db.get_value("Payment Gateway", payment_gateway, "title")
					mode_of_payment.icon = frappe.db.get_value("Payment Gateway", payment_gateway, "icon")

				mode_of_payment.save()


			webshop_settings = frappe.get_single("Webshop Settings")
			if not webshop_settings.authorized_modes_of_payment:
				webshop_settings.append("authorized_modes_of_payment", {
					"mode_of_payment": mode_of_payment.name
				})
				webshop_settings.ignore_mandatory = True
				webshop_settings.save()