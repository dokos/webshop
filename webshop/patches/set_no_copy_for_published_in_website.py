import frappe


def execute():
	if frappe.db.exists("Custom Field", "Item-published_in_website"):
		doc = frappe.get_doc("Custom Field", "Item-published_in_website")
		doc.no_copy = True
		doc.save()
