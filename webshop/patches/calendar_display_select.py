import frappe

def execute():
	for resource in frappe.get_all("Website Item", filters={"hide_calendar_from_guests": 1}):
		frappe.db.set_value("Website Item", resource.name, "calendar_display", "Only show calendar to logged in users")