# Copyright (c) 2021, Frappe Technologies Pvt. Ltd. and Contributors
# License: GNU General Public License v3. See license.txt

no_cache = 1

import frappe
from bookings.bookings.doctype.item_booking.item_booking import get_availabilities

from webshop.webshop.shopping_cart.cart import get_cart_quotation


def get_context(context):
	context.no_cache = 1
	context.body_class = "product-page"
	context.update(get_cart_quotation())


@frappe.whitelist(allow_guest=True)
def get_availabilities_for_cart(item: str, start, end, uom: str | None = None):
	# Check that the item is bookable and not hidden to Guest
	website_item = frappe.db.get_value(
		"Website Item",
		{"item_code": item},
		["published", "enable_item_booking", "calendar_display"],
		as_dict=True,
	)
	if not (website_item and website_item.published and website_item.enable_item_booking):
		raise frappe.PermissionError
	if (
		frappe.session.user == "Guest"
		and website_item.calendar_display == "Only show calendar to logged in users"
	):
		return []
	return get_availabilities(item=item, start=start, end=end, uom=uom)
