## Dokos Webshop

![Dokos Webshop](webshop.png)

## Installation
1. [Install bench/docli](https://gitlab.com/dokos/docli).
2. [Install Dokos](https://doc.dokos.io/dodock/installation).
3. Once Dokos is installed, add the `webshop` app to your bench by running

    ```sh
    $ bench get-app webshop
    ```
4. After that, you can install the webshop app on the required site by running
    ```sh
    $ bench --site sitename install-app webshop
    ```

#### License
GNU GPL V3. (See [LICENSE](LICENSE) for more information).
